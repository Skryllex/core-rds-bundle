<?php
/**
 * @file
 * Definition translatable master table.
 */
namespace SylrSyksSoftSymfony\CoreBundle\RDS\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractMasterTableEntity;
use SylrSyksSoftSymfony\CoreBundle\Model\TranslatableModelTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass()
 */
abstract class AbstractTranslatableMasterTableEntity extends AbstractMasterTableEntity implements Translatable
{
    use TranslatableModelTrait;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="The field is required.", groups={"MasterTable"})
     * @Assert\Length(
     *      max=255,
     *      maxMessage="The title is too long.",
     *      groups={"MasterTable"}
     * )
     * @Gedmo\Versioned()
     * @Gedmo\Translatable()
     */
    protected $title;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=10000, nullable=true)
     * @Assert\Length(
     *      max=10000,
     *      maxMessage="The title is too long.",
     *      groups={"MasterTable"}
     * )
     * @Gedmo\Versioned()
     * @Gedmo\Translatable()
     */
    protected $description;
}