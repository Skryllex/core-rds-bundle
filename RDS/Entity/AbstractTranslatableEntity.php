<?php
/**
 * @file
 * Defining the basic entity with common properties for entities.
 */
namespace SylrSyksSoftSymfony\CoreBundle\RDS\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Translatable;
use SylrSyksSoftSymfony\CoreBundle\Entity\AbstractEntity;
use SylrSyksSoftSymfony\CoreBundle\Model\TranslatableModelTrait;

/**
 * @ORM\MappedSuperclass()
 */
abstract class AbstractTranslatableEntity extends AbstractEntity implements Translatable
{
    use TranslatableModelTrait;
}