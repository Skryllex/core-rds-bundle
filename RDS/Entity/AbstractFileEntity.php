<?php
/**
 * @file
 * Entity file.
 */
namespace SylrSyksSoftSymfony\CoreBundle\RDS\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use SylrSyksSoftSymfony\CoreBundle\Model\AbstractFileModel;

/**
 * @ORM\MappedSuperclass()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable()
 */
abstract class AbstractFileEntity extends AbstractFileModel
{
    /**
     *
     * @var string
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    protected $filename;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="mime_type", type="string", length=255, nullable=true)
     */
    protected $mimeType;

    /**
     *
     * @var integet
     *
     * @ORM\Column(name="path", type="integer")
     */
    protected $length;

    /**
     *
     * @var integet
     *
     * @ORM\Column(name="path", type="integer")
     */
    protected $chunkSize;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=50)
     */
    protected $md5;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    protected $deletedAt;
}