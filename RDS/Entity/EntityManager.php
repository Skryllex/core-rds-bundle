<?php
/**
 * @file
 * Entity manager.
 */
namespace SylrSyksSoftSymfony\CoreBundle\RDS\Entity;

use Sonata\CoreBundle\Model\BaseEntityManager;

final class EntityManager extends BaseEntityManager
{

    /**
     *
     * {@inheritDoc}
     *
     * @see \Sonata\CoreBundle\Model\BaseManager::save()
     */
    public function save($entity, $andFlush = TRUE)
    {
        $success = FALSE;
        $this->checkObject($entity);

        $this->getConnection()->beginTransaction();

        try {
            $this->getObjectManager()->persist($entity);
            $this->getConnection()->commit();

            if ($andFlush) {
                $this->getObjectManager()->flush();
            }

            $success = TRUE;
        } catch (Exception $e) {
            $success = FALSE;
            $this->getConnection()->rollBack();
        }

        return $success;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Sonata\CoreBundle\Model\BaseManager::delete()
     */
    public function delete($entity, $andFlush = TRUE)
    {
        $success = FALSE;
        $this->checkObject($entity);

        $this->getConnection()->beginTransaction();

        try {
            $this->getObjectManager()->remove($entity);
            $this->getConnection()->commit();

            if ($andFlush) {
                $this->getObjectManager()->flush();
            }

            $success = TRUE;
        } catch (Exception $e) {
            $success = FALSE;
            $this->getConnection()->rollBack();
        }

        return $success;
    }
}