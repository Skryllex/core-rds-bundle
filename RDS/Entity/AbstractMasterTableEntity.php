<?php
/**
 * @file
 * Master table entity.
 */
namespace SylrSyksSoftSymfony\CoreBundle\RDS\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use SylrSyksSoftSymfony\CoreBundle\Model\AbstractMasterTableModel;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\MappedSuperclass()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @DoctrineAssert\UniqueEntity(fields={"code", "title", "slug"}, message="The values already exist.", groups={"MasterTable"})
 * @Gedmo\Loggable()
 */
abstract class AbstractMasterTableEntity extends AbstractMasterTableModel
{

    /**
     *
     * @var string
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, unique=true)
     * @Assert\NotBlank(message="The field is required.", groups={"MasterTable"})
     * @Assert\Length(
     *      min=3,
     *      max=50,
     *      minMessage="The code is too short.",
     *      maxMessage="The code is too long.",
     *      groups={"MasterTable"}
     * )
     * @Gedmo\Versioned()
     */
    protected $code;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="The field is required.", groups={"MasterTable"})
     * @Assert\Length(
     *      max=255,
     *      maxMessage="The title is too long.",
     *      groups={"MasterTable"}
     * )
     * @Gedmo\Versioned()
     */
    protected $title;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=10000, nullable=true)
     * @Assert\Length(
     *      max=10000,
     *      maxMessage="The title is too long.",
     *      groups={"MasterTable"}
     * )
     * @Gedmo\Versioned()
     */
    protected $description;

    /**
     * @ORM\Column(name="slug", type="string", length=128, unique=true)
     * @Gedmo\Slug(fields={"title"})
     */
    protected $slug;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Assert\DateTime()
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    protected $deletedAt;
}