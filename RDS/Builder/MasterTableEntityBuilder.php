<?php
/**
 * @file
 * Master Table builder.
 */
namespace SylrSyksSoftSymfony\CoreBundle\RDS\Builder;

use SylrSyksSoftSymfony\CoreBundle\Builder\AbstractObjectBuilder;
use SylrSyksSoftSymfony\CoreBundle\Builder\MasterTable\MasterTableBuilderInterface;
use SylrSyksSoftSymfony\CoreBundle\Builder\MasterTable\MasterTableBuilderTrait;

final class MasterTableEntityBuilder extends AbstractObjectBuilder implements MasterTableBuilderInterface
{
    use MasterTableBuilderTrait;
}